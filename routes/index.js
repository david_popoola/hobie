var express = require('express');
var router = express.Router();
var User = require('../models/user.js');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var flash = require('connect-flash');
const nodemailer = require('nodemailer');


function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't, redirect them to the home page
    req.flash("success", "You have to be logged in to add hobbies, fam. So voila, off you go back to the homepage");
    res.redirect('/login');
}

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Hobie the App' });
});

router.get('/login', function (req, res, next) {
	if (!req.isAuthenticated()) {
		res.render('login', {title: 'Login and create more hobbies'});
	} else{
		res.render('index', {title: 'Hobie the App'});
	}
});

router.get('/signup', function (req, res, next) {
	res.render('signup', {title: 'Signup and track your hobbies'});
});

router.post('/signup', function (req, res, next) {
	var email = req.body.email;
	var password = req.body.password;

	req.checkBody('email', 'Your email field cannot be blank').notEmpty();
	req.checkBody('email', 'Please enter a valid email address').isEmail();
	req.checkBody('password', 'Password has to be 6 - 20 characters long').len(6, 20);

	var errors = req.validationErrors();
	if (errors) {
		res.render('signup', { errors: errors });
	    return;
	} else {
	    // normal processing here
	    var newUser = new User({
	    	email: email,
	    	password: password,
	    	hobbies: []
	    })

	    User.createUser(newUser, function (err, user) {
	    	if(err) throw err;
	    	console.log(user);
	    })

	    // console.log("Dodo is my favorite food");
	    res.location('/');
	    res.redirect('/');
	}
})

passport.serializeUser(function(user, done) {
	done(null, user.id);
});

passport.deserializeUser(function(id, done) {
	User.getUserById(id, function(err, user) {
    	done(err, user);
  	});
});

passport.use(new LocalStrategy({}, 
	function(email, password, done) {
	    User.getUserByEmail(email, function (err, user) {
	      if (err) { return done(err); }
	      if (!user) {
	      	// console.log('Unknown User');
	        return done(null, false, { message: 'Incorrect username.' });
	      }
	      User.comparePassword(password, user.password, function (err, isMatch) {
	      	if(err) throw err;
	      	if(isMatch){
	      		return done(null, user);
	      	} else{
	      		// console.log("Incorrect password");
	      		return done(null, false, { message: 'Incorrect password.' });
	      		return done(null, user);
	      	}
	      })
	      });
	}
));

router.post('/login', passport.authenticate('local', { failureRedirect: '/login', successRedirect: '/add', failureFlash: true }), function (req, res, next) {
	req.flash('success', "Welcome man");
	res.redirect('/add');
})

router.get('/logout', function(req, res){
    req.logOut();
    // console.log("You logged out!");
    req.flash('success', "You have successfully logged out");
    res.redirect('/login');
});

router.get('/add', isLoggedIn, function (req, res, next) {
	res.render('add');
})

router.post('/add', function (req, res, next) {
	var hobby = req.body.hobby;

	User.findByIdAndUpdate(req.user.id, { "$push": { "hobbies": hobby } }, { "new": true, "upsert": true }, function (err, user) {
        if (err) throw err;
        console.log(user);
        req.flash("success", "We added the hobby to your hobby list");
        var accountSid = 'AC1269dfc5305c2bf0cdd5f2191a85475a'; 
			 var authToken = '1dc32d64368b8db8f07b42bb0e7ce4b8'; 
			 
			//require the Twilio module and create a REST client 
			var client = require('twilio')(accountSid, authToken); 
			 
			client.messages.create({ 
			    to: "+2348124916894", 
			    from: "+14695186025",
			    body: "Hello there, your newly created hobby is " + hobby, 
			}, function(err, message) { 
				if(err) {
					throw err;
				} else {
					// console.log(message.sid);
				}
			});
        res.redirect('/add');
        // Generate test SMTP service account from ethereal.email
		// Only needed if you don't have a real mail account for testing
        // nodemailer.createTestAccount((err, account) => {

			    // create reusable transporter object using the default SMTP transport
			    let transporter = nodemailer.createTransport({
			        host: 'smtp.gmail.com',
			        port: 587,
			        secure: false, // true for 465, false for other ports
			        auth: {
			            user: 'davieedpopoola@gmail.com', // generated ethereal user
			            pass: 'wonderfulpapami'  // generated ethereal password
			        }
			    });

			    // setup email data with unicode symbols
			    let mailOptions = {
			        from: '"Hobie family 👻" <davieedpopoola@gmail.com>', // sender address
			        to: req.user.email, // list of receivers
			        subject: 'Hello, you just created a new hobby', // Subject line
			        text: 'Hello there, your newly created hobby is ' + hobby, // plain text body
			        // html: '<b>Hello world?</b>' // html body
			    };

			    // send mail with defined transport object
			    transporter.sendMail(mailOptions, function(error, info) {
			        if (error) {
			            throw error;
			        }
			        // console.log('Message sent: %s', info.messageId);
			        // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
			    });
			 hobby = '';
    });
})

router.get('/hobbies', isLoggedIn, function (req, res, next) {
	User.findById(req.user.id, function (err, user) {
		if (err) {throw err}
			else{
				var hobbs = user.hobbies;
				// console.log(user);
				// console.log(hobbs);
				res.render('hobbies', {hobbs: hobbs});
			}
	})
})

module.exports = router;
