var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var config = require('../config/config');
mongoose.connect(config.DBURI);
var db = mongoose.connection;

// User schema
var userSchema = mongoose.Schema({
	email: {
		type: String
	},
	password: {
		type: String,
		bcrypt: true
	},
	hobbies: Array
});


var User = module.exports = mongoose.model("User", userSchema);

module.exports.comparePassword = function(cP, hash, callback){
	bcrypt.compare(cP, hash, function (err, isMatch) {
		if(err) return callback(err);
		callback(null, isMatch);
	})
}

module.exports.getUserById = function (id, callback) {
	User.findById(id, callback);
}

module.exports.getUserByEmail = function (email, callback) {
	var query = {email: email};
	User.findOne(query, callback);
}

module.exports.createUser = function (newUser, callback) {
	bcrypt.genSalt(10, function(err, salt) {
    	bcrypt.hash(newUser.password, salt, function(err, hash) {
        // Store hash in your password DB.
        	if(err) throw err;
        	newUser.password = hash;
    		newUser.save(callback);    	
		});
	});
}

module.exports.updateUser = function (id, callback) {
	// var query = { _id: id };
	User.update({ _id: id }, { $set: { hobbies: ['look'] }}, callback);
}